Welcome to Cells: Unified Cellular Automata Modules

Website: https://cenysor.neocities.org/cells

To run this program you need to have python 3 installed (tested with Python
3.10.6)
as well as the python modules "pygame" and "matplotlib".

You also need to have ffmpeg installed if you want to export video files.

You can use the following command line arguments:

--hw x      set the display size, takes two positive integers in the format
            <height>x<width> as the argument x (e.g. 200x200). Defaults to
            the current display height and half of its width

-v          begin frame recording for video export at startup

-p x        set the display size of a single cell, takes a positive integer as
            the argument x, sets the cell size to x times x pixels (more cells
            per screen for lower integers). The default value depends on the
            ruletype
            
-s          enable statistics and export them as .png

-t          export summary as .txt

-f x        enable full screen, takes 'w' or 'q' as the mandatory argument x. 
            'w' for widescreen and 'q' for a square window. If -f is not set
            the display defaults to half of the current screen resolution.
            
-m x        set maximum number of generations before quitting, takes a positive
            integer as the argument x. An export will be triggered when quitting
            (see keyboard shortcut [x] below)
            
-d          quit the simulation when the change in the number of different
            cells states becomes less than 1%. Statistics is enabled by default
            (see -s). An export will be triggered when quitting (see keyboard    
            shortcut [x] below)
            
--fill x    set the percentage of cells with non-quiescent state at the start of
            the simulation. Takes a positive float between 0 and 1 as the 
            argument x. The default value depends on the ruletype
            
--flicker   turns on the "anti-flicker" mode at startup. It displays only every
            second generation of the grid. Turning it on also applies to frame
            recording
            
--states x  set the number of possible cell states, takes a positive integer as
            the argument x
            
-n          set the neighborhood for the transition rule, neighborhoods are 
            defined in the file neighborhoods.py
            
--save x    takes a filename (without ".cells" file extension) as the argument x
            to save the rule in ./saves. You do not need to provide a ruletype
            (see below)
            
-i x        takes a filename (without ".cells" file extension) in ./saves as the
            argument x to load the saved rule
            
The last argument defines the ruletype. Some ruletypes support special command
line arguments (placed before the ruletype argument). The following ruletypes are
currently implemented:

Anisotropic simulate randomly generated non-isotropic cellular automata, with
            arbitrary neighborhoods and an arbitrary number of states
            
-z          the "zero-state" is quiescent by default. If -z is set, the "zero-
            state" is randomized  
            
--tame x    takes a float between 0 and 1 as the argument x.
            Higher values of x make the rule less "explosive"
            
-r x        takes a float between 0 and 1 as the argument x. The fraction x of
            cells will be changed to a random state each generation


Isotropic   simulate randomly generated isotropic cellular automata, with
            arbitrary neighborhoods and an arbitrary number of states

-z          the "zero-state" is quiescent by default. If -z is set, the "zero-
            state" is randomized

--tame x    takes a float between 0 and 1 as the argument x.
            Higher values of x make the rule less "explosive"

-r x        takes a float between 0 and 1 as the argument x. The fraction x of
            cells will be changed to a random state each generation

--radius x  set the radius of rotational symmetric neighborhoods, defaults to 1


Totalistic  simulate Life-like cellular automata, with arbitrary neighborhoods
            and an arbitrary number of states
            
--bs x      takes a transition rule in the b/s notation as the argument x.
            Defaults to 3/23 (Conway's Game of Life)
            
-r x        takes a float between 0 and 1 as the argument x. The fraction x of
            cells will be changed to a random state each generation
            
            
Genetic     genetic algorithm for Life-like cellular automata

-l x        set the limits u and v (see website), takes a string of two
            positive integers between 0 and 9 as the argument x. E.g. '4/5'
            sets u = 4 and v = 5. If not specified defaults to random values
            for u and v
            
-r x        takes a float between 0 and 1 as the argument x. The fraction x of
            cells will be mutated
            

Logical     cellular logic simulation, introduction:
            https://cenysor.neocities.org/cells
            
--palette x takes red, green or blue as the argument x. Sets the base color to x
            
            
BioChem     define complex behavior and movement for single cells  to simulate
            biochemical processes for example      

                        
Example:

~$ python3 display.py -f w -v --bs 36/23 Totalistic

This will start the simulation of the Life-like CA "HighLife" (rulestring:
B36/23) in full screen (widescreen) mode and will begin frame recording at
startup.


While the simulation is running you can use the following inputs:

mouse buttons:

[left click]    on a cell to output its state to the console

[right click]   to place a cell with a state defined by console input

[middle click]  to copy a cell, then right click to place the cell

keyboard shortcuts:

[h]     show usage of command line arguments and keyboard shortcuts

[v]     start/stop frame recording for video export. If stopped the recorded
        frames will be converted to .mp4 (see also [x])
        
[r]     reset the simulation. This will also clear the statistics and video
        frames for export
        
[x]     if the command line arguments -v, -s or -t have been set, or frame
        recording has been started with [v] this will export:
        recorded frames as .mp4
        statistics as .png
        summary as .txt
        
[f]     turn off/on the "anti-flicker" mode (see --flicker under command line
        arguments above)
        
[0]     toggle the color scheme

[s]     save rule to file (see --save above)

[SPACE] pause/unpause the simulation

[<]     pause and step to the next generation
