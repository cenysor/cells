import random
import copy
from neighborhoods import *


class Rule:
    """Base class for cellular automaton rules"""

    def __init__(self, height: int, width: int, pixel: int, colors: list, neighborhood: str):
        self.pixel = pixel
        self.height = height // self.pixel
        if (self.height * self.pixel) % 2 != 0:
            self.height -= 1
        self.width = width // self.pixel
        if (self.width * self.pixel) % 2 != 0:
            self.width -= 1
        self.colors = colors
        self.neighborhood = neighborhood
        self.coord()
        self.empty()

    def coord(self):
        """look up neighborhood string"""

        self.coordlist = globals()[self.neighborhood]
        self.center = self.coordlist.index((0, 0))

    def empty(self):
        """Create empty grid and nextgrid"""

        self.grid = [[None for x in range(self.width)] for y in range(self.height)]
        self.nextgrid = copy.deepcopy(self.grid)

    def randomize_grid(self, states: list, fill: float):
        """Populate grid with random states and density of fill, rest will be state[0]"""

        for y, row in enumerate(self.grid):
            for x, state in enumerate(row):
                if random.random() < fill:
                    self.grid[y][x] = random.choice(states[1:])
                else:
                    self.grid[y][x] = states[0]

    def return_neighbors(self, coordlist: tuple, y: int, x: int) -> list:
        """Return list of cells in neighborhood of cell at grid[y][x]"""

        return [self.grid[(y + n_y) % self.height][(x + n_x) % self.width] for n_y, n_x in coordlist]


class Cell:
    """Base class of cell"""

    def __init__(self):
        self.state = None
