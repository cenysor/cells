import random
from cells import *

# Rules for BioChem molecules can be defined here


class Molecule(Cell):
    """Base class of BioChem rules"""

    def __init__(self):
        super().__init__()


class Cytosol(Molecule):
    """Cytosol fills the interior of a cell"""

    def __init__(self):
        super().__init__()

    def __str__(self):
        return 'Cytosol'

    def __repr__(self):
        return 'Cytosol'

    def action(self, neighbors: list, new_center: Molecule, new_neighbors: list):
        if new_center is None:
            n = [neighbor for neighbor in neighbors if neighbor is not None]
            new_n = [neighbor for neighbor in new_neighbors if neighbor is not None]
            if len(new_n) >= len(n):
                return((True, None))

        return((False, None))


class DNA(Molecule):
    """DNA stores information to build molecules"""

    def __init__(self):
        super().__init__()
        self.state = random.choice(['None', 'grow'])

    def __str__(self):
        return 'DNA'

    def __repr__(self):
        return 'DNA ' + self.state

    def action(self, neighbors: list, new_center: Molecule, new_neighbors: list):
        if str(new_center) == 'Cytosol':
            n = [neighbor for neighbor in neighbors if str(neighbor) == 'DNA']
            if len(n) not in (1, 2):
                return((True, None))

        return((False, None))


class Ribosome(Molecule):
    """Ribosomes translate DNA into molecules"""

    def __init__(self):
        super().__init__()

    def __str__(self):
        return 'Ribosome'

    def __repr__(self):
        return 'Ribosome'

    def action(self, neighbors: list, new_center: Molecule, new_neighbors: list):
        g_cand = [neighbor.state for neighbor in neighbors if str(neighbor) == 'DNA']
        t_cand = [neighbor for neighbor in neighbors if str(neighbor) == 'Cytosol']
        if len(g_cand) > 0 and len(t_cand) > 0 and random.random() < 0.1:
            target = random.choice(t_cand)
            gene = random.choice(g_cand)
            if gene == 'grow':
                return ((False, [(target, Vesicle('Cytosol'))]))

        else:
            if str(new_center) == 'Cytosol':
                new_n = [neighbor for neighbor in new_neighbors if str(neighbor) == 'DNA']
                if len(new_n) >= 1:
                    return((True, None))

        return((False, None))


class Vesicle(Molecule):
    """Vesicles transport molecules to the cell membrane"""

    def __init__(self, molecule):
        super().__init__()
        self.state = molecule

    def __str__(self):
        return 'Vesicle'

    def __repr__(self):
        return 'Vesicle'

    def action(self, neighbors: list, new_center: Molecule, new_neighbors: list):
        cand = [neighbor for neighbor in neighbors if str(neighbor) == 'None']
        if len(cand) > 0:
            target = random.choice(cand)
            return ((False, [(target, globals()[self.state]()),
                             (self, Cytosol())]))
        else:
            if str(new_center) == 'Cytosol':
                return ((True, None))

        return ((False, None))
