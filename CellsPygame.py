import pygame
import getopt
import sys
from display import *

pygame.init()
helptext()
pygame.display.set_caption('Cells')

height = pygame.display.Info().current_h
width = pygame.display.Info().current_w // 2
copy = None
scheme = 0
pause = False
step = False

opts, args = getopt.getopt(sys.argv[1:], 'vstdzm:f:l:p:n:r:i:', ['fill=', 'flicker', 'states=', 'tame=', 'bs=',
                                                                 'palette=', 'hw=', 'save=', 'radius='])

params = {}
display_params = {'imp': None,
                  'vid': False,
                  'statistic': False,
                  'text': False,
                  'temp_dir': Path.cwd() / 'temp',
                  'export_dir': Path.cwd() / 'export',
                  'saves_dir': Path.cwd() / 'saves',
                  'antiflicker': False,
                  'save': None}

for opt, arg in opts:
    if opt == '-v':
        display_params['vid'] = True
    elif opt == '-p':
        params['pixel'] = int(arg)
    elif opt == '--fill':
        params['fill'] = float(arg)
    elif opt == '-s':
        display_params['statistic'] = True
    elif opt == '-t':
        display_params['text'] = True
    elif opt == '-f':
        if arg == 'w':
            width = pygame.display.Info().current_w
            height = pygame.display.Info().current_h
        elif arg == 'q':
            width = pygame.display.Info().current_h
            height = pygame.display.Info().current_h
    elif opt == '--hw':
        height_str, width_str = arg.split('x')
        height, width = int(height_str), int(width_str)
    elif opt == '--flicker':
        display_params['antiflicker'] = True
    elif opt == '-l':
        params['uv'] = arg
    elif opt == '-m':
        display_params['maxgen'] = int(arg)
    elif opt == '-d':
        display_params['statistic'] = True
        display_params['mindelta'] = True
    elif opt == '--states':
        params['states'] = int(arg)
    elif opt == '-n':
        params['neighborhood'] = arg
    elif opt == '-z':
        params['zero'] = False
    elif opt == '--tame':
        params['tame'] = float(arg)
    elif opt == '-r':
        params['rand'] = float(arg)
    elif opt == '--bs':
        params['bs'] = arg
    elif opt == '--radius':
        params['radius'] = int(arg)
    elif opt == '--palette':
        params['palette'] = arg
    elif opt == '--save':
        display_params['save'] = arg
    elif opt == '-i':
        try:
            file = shelve.open(str(display_params['saves_dir'] / Path(arg + '.cells')))
            display_params['imp'] = display_params['saves_dir'] / Path(arg + '.cells')
            args.append(file['name'])
            file.close()
        except:
            print('rule not found, loading "Totalistic" instead')
            args.append('Totalistic')

makedir(display_params['temp_dir'])

# initialize rule

rule = globals()[args[-1]](height, width, **params)

# get colors

colors = rule.colors[scheme]

# initialize pygame window

win = pygame.display.set_mode((rule.width * rule.pixel, rule.height * rule.pixel))

setup(rule, imp=display_params['imp'])
for y in range(rule.height):
    for x in range(rule.width):
        pygame.draw.rect(win, colors[str(rule.grid[y][x])], (x * rule.pixel, y * rule.pixel, rule.pixel, rule.pixel))

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            export(display_params['vid'],
                   display_params['text'],
                   display_params['statistic'],
                   display_params['temp_dir'],
                   display_params['export_dir'],
                   display_params['saves_dir'],
                   display_params['save'],
                   rule)
            sys.exit()
        if event.type == pygame.MOUSEBUTTONDOWN:
            mx, my = pygame.mouse.get_pos()
            if event.button == 1:
                print(repr(rule.grid[my // rule.pixel][mx // rule.pixel]))
            elif event.button == 3:
                rule.right_click(my // rule.pixel, mx // rule.pixel, copy)
                pygame.draw.rect(win, colors[str(rule.grid[my // rule.pixel][mx // rule.pixel])],
                                 ((mx // rule.pixel) * rule.pixel, (my // rule.pixel) * rule.pixel,
                                  rule.pixel, rule.pixel))
                pygame.display.update()
                copy = None
            elif event.button == 2:
                copy = rule.grid[my // rule.pixel][mx // rule.pixel]
                print('copied', repr(copy))
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_0:
                scheme = (scheme + 1) % len(rule.colors)
                colors = rule.colors[scheme]
                for y in range(rule.height):
                    for x in range(rule.width):
                        pygame.draw.rect(win, colors[str(rule.grid[y][x])],
                                         (x * rule.pixel, y * rule.pixel, rule.pixel, rule.pixel))
            if event.key == pygame.K_v:
                if display_params['vid']:
                    export(display_params['vid'],
                           display_params['text'],
                           display_params['statistic'],
                           display_params['temp_dir'],
                           display_params['export_dir'],
                           display_params['saves_dir'],
                           display_params['save'],
                           rule)
                display_params['vid'] = not display_params['vid']
            if event.key == pygame.K_r:
                setup(rule, imp=display_params['imp'])
                for y in range(rule.height):
                    for x in range(rule.width):
                        pygame.draw.rect(win, colors[str(rule.grid[y][x])],
                                         (x * rule.pixel, y * rule.pixel, rule.pixel, rule.pixel))
                clean(display_params['temp_dir'])
            if event.key == pygame.K_x:
                export(display_params['vid'],
                       display_params['text'],
                       display_params['statistic'],
                       display_params['temp_dir'],
                       display_params['export_dir'],
                       display_params['saves_dir'],
                       display_params['save'],
                       rule)
            if event.key == pygame.K_h:
                helptext()
            if event.key == pygame.K_f:
                display_params['antiflicker'] = not display_params['antiflicker']
            if event.key == pygame.K_s:
                name = input('filename: ')
                save_rule(rule, name, display_params['saves_dir'])
            if event.key == pygame.K_SPACE:
                pause = not pause
                if pause:
                    print('pause at gen', gen)
                else:
                    print('continue')
            if event.key == pygame.K_LESS:
                step = True
                pause = False
                print('step', gen)

    if not pause:
        gen, vid_gen, run, paint_list = main_loop(rule, **display_params)
        for paint in paint_list:
            py, px, cell = paint
            pygame.draw.rect(win, colors[str(cell)], (px * rule.pixel, py * rule.pixel, rule.pixel, rule.pixel))
        if run:
            pygame.display.update()
            if vid_gen > 0:
                filename = 'temp_image' + str(vid_gen) + '.png'
                pygame.image.save(win, display_params['temp_dir'] / Path(filename))
        elif run is False:
            break
        if step:
            step = False
            pause = True
