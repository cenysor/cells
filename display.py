import subprocess
import os
from pathlib import Path
import time
import matplotlib.pyplot as plt
import numpy as np
import shelve
from rules import *

# Common display module


def makedir(path: Path):
    if not path.is_dir():
        os.makedirs(path)


def convert(fname: str, temp_dir: Path, export_dir: Path):
    """convert images in temp directory into mp4 video"""
    p = subprocess.Popen(['ffmpeg',
                          '-y',
                          '-i', str(temp_dir / 'temp_image%d.png'),
                          '-pix_fmt', 'yuv420p',
                          str(export_dir / (fname + '.mp4'))])
    p.wait()


def clean(temp_dir: Path):
    """clear temp directory and set image numbering for video conversion to 0"""
    global vid_gen

    for imagefile in temp_dir.glob('temp_image*.png'):
        os.unlink(imagefile)

    vid_gen = 0


def setup(rule: Rule, imp=None):
    """initialize variables for simulation run, reload imported rule and reset the simulation"""
    global gen
    global vid_gen
    global stat
    global n
    global delta
    global start_time

    gen = 0
    vid_gen = 0
    stat = {}
    n = [0]
    delta = False
    if imp is not None:
        imp_file = shelve.open(str(imp))
        rule.load(imp_file)
        imp_file.close()
    else:
        rule.reset()
    start_time = time.time()


def export(vid: bool, text: bool, statistic: bool, temp_dir: Path, export_dir: Path, saves_dir: Path, save: str,
           rule: Rule):
    """export video, text summery and statistics"""
    if vid or text or statistic:
        end_time = time.time()
        makedir(export_dir)
        fname = str(round(end_time))
    if vid:
        convert(fname, temp_dir, export_dir)
        clean(temp_dir)
        print('\n' + 'exported:')
        for k, v in rule.text().items():
            print(k + ':', v)
    if text:
        textfile = open(export_dir / (fname + '.txt'), 'a')
        for k, v in rule.text().items():
            textfile.write(k + ': ' + v + '\n')
        textfile.write('average time per generation: ' + str((end_time - start_time) / gen) + ' s')
        textfile.close()
    if statistic:
        gentext = '\ngeneration: ' + str(gen)
        print(gentext)
        if text:
            textfile = open(export_dir / (fname + '.txt'), 'a')
            textfile.write('\n' + gentext)
        stat_list = []
        for state, v in stat.items():
            if state not in ('None', '0'):
                plt.plot(np.array(v[1][1:]))
            if v[1][-1] != 0:
                stat_list.append((state, round((v[1][-1] / n[-1]) * 100, 2)))
        stat_list.sort(key=lambda s: s[1], reverse=True)
        for i in stat_list:
            line = i[0] + ': ' + str(i[1]) + '%'
            print(line)
            if text:
                textfile.write('\n' + line)
        if text:
            textfile.close()
        plt.xlabel('generation')
        plt.ylabel('cell count')
        plt.savefig(export_dir / (fname + '.png'))
        plt.clf()
    if save is not None:
        save_rule(rule, save, saves_dir)


def save_rule(rule: Rule, save_name: str, saves_dir: Path):
    """save current rule with .cells extension in saves directory"""
    makedir(saves_dir)
    save_file = shelve.open(str(saves_dir / (save_name + '.cells')))
    save = rule.save()
    for k, v in save.items():
        save_file[k] = v
    save_file.close()
    print('exported rule to', str(saves_dir / (save_name + '.cells')))


def main_loop(rule: Rule, antiflicker=False, maxgen=None, mindelta=False, vid=False, text=False, statistic=False,
              temp_dir=Path('./temp'), export_dir=Path('./export'), saves_dir=Path('./saves'), save=None, imp=None):
    """run the simulation"""
    global gen
    global vid_gen
    global delta
    show = True

    if gen == maxgen or (mindelta and gen >= 43 and delta):
        export(vid, text, statistic, temp_dir, export_dir, saves_dir, save, rule)
        return gen, vid_gen, False, []

    if statistic:
        n.append(0)
        delta = True
        for w in stat.values():
            if mindelta:
                if gen >= 42 and w[1][-1] != 0 and (w[1][-1] / n[-2]) > 0.005:
                    if not round(((w[1][-1] - w[1][-41]) / w[1][-41]) * 100) in range(-1, 2):
                        delta = False
            w[1].append(0)
        if mindelta and gen == 42 and delta:
            setup(rule, imp)
            clean(temp_dir)

    paint_list = []

    for y in range(rule.height):
        for x in range(rule.width):
            for paint in rule.tic(y, x):
                paint_list.append(paint)
            if statistic:
                key = repr(rule.grid[y][x])
                stat.setdefault(key, [str(rule.grid[y][x]), [0] * (gen + 1)])
                stat[key][1][-1] += 1
                n[-1] += 1

    if antiflicker:
        if gen % 2 != 0:
            show = None
        else:
            if vid:
                vid_gen += 1
    else:
        if vid:
            vid_gen += 1

    gen += 1

    rule.swap()

    return gen, vid_gen, show, paint_list


def helptext():
    """display help text"""
    readmefile = open('README.txt')
    readmetext = readmefile.read()
    print('\n' + readmetext)
