# Rules for Logical CA
# Feel free to experiment
# Every rule has to output a binary 8 bit tuple for every possible 8 bit tuple input (x)

import random

### generate random rule ###

random_rule = []

for i in range(256):
    bits = []
    for i in range(8):
        bits.append(random.randint(0, 1))
    random_rule.append(tuple(bits))

random_rule_0 = []

for i in range(256):
    bits = []
    for i in range(8):
        if random.random() < 0.9:
            bits.append(0)
        else:
            bits.append(1)
    random_rule_0.append(tuple(bits))

### Example Rules ###

# opcodes



# ALU

def one_bit_adder(x):
    s = x[2] + x[4]

    if s == 2:
        return((1, 0, 0, 0, 0, 0, 0, 0))

    else:
        return((0, 0, s, 0, 0, 0, 0, 0))


def two_bit_adder(x):
    """add two 2-bit numbers (+ carry), so that c1s1s0 = a1a0 + b1b0 + c0

             c0 b1 b2
    i/o map: a0    s1
             a1 s0 c1
    """
    s = x[7] + (x[1] * 2 + x[0]) + (x[6] * 2 + x[5])

    if s > 3:
        c_1 = 1
        s = s % 4
    else:
        c_1 = 0

    if s > 1:
        s_1 = 1
        s = s % 2
    else:
        s_1 = 0

    if s == 1:
        s_0 = 1
    else:
        s_0 = 0

    return((0, 0, s_0, c_1, s_1, 0, 0, 0))


def add_one(x):
    out = []
    c = 1

    for i in x:
        s = i + c
        if s == 2:
            c = 1
        else:
            c = 0
        out.append(s % 2)
    
    return(tuple(out))


def primes(x):
    inp = 0
    for i in range(8):
        inp += x[i] * 2 ** i
    
    while True:
        inp += 1
        prime = True
        for i in range(2, inp):
            if inp % i == 0:
                prime = False
                break
        if prime == True:
            break

    if inp - 256 >= 0:
        return((0, 0, 0, 0, 0, 0, 0, 0))

    out = [0, 0, 0, 0, 0, 0, 0, 0]

    for i in range(7, -1, -1):
        if inp - 2 ** i >= 0:
            out[i] = 1
            inp -= 2 ** i

    return(tuple(out))            


def circular_shift_1(x):
    return((x[7], x[0], x[1], x[2], x[3], x[4], x[5], x[6]))


def circular_shift_2(x):
    return((x[6], x[7], x[0], x[1], x[2], x[3], x[4], x[5]))


def circular_shift_3(x):
    return((x[5], x[6], x[7], x[0], x[1], x[2], x[3], x[4]))


# clocks


def clock_16(x):
    if x == (0, 0, 0, 0, 0, 0, 0, 0):
        return((1, 0, 0, 0, 0, 0, 0, 0))
    else:
        return((x[7], x[0], x[1], x[2], x[3], x[4], x[5], x[6]))


def clock_8(x):
    if x == (0, 0, 0, 0, 0, 0, 0, 0):
        return((1, 0, 0, 0, 0, 0, 0, 0))
    else:
        return((x[6], x[7], x[0], x[1], x[2], x[3], x[4], x[5]))


def clock_16_3(x):
    if x == (0, 0, 0, 0, 0, 0, 0, 0):
        return((1, 0, 0, 0, 0, 0, 0, 0))
    else:
        return((x[5], x[6], x[7], x[0], x[1], x[2], x[3], x[4]))


# power


def power_off(x):
    return((0, 0, 0, 0, 0, 0, 0, 0))


def power_0(x):
    return((1, 0, 0, 0, 0, 0, 0, 0))


def power_1(x):
    return((0, 1, 0, 0, 0, 0, 0, 0))


def power_2(x):
    return((0, 0, 1, 0, 0, 0, 0, 0))


def power_3(x):
    return((0, 0, 0, 1, 0, 0, 0, 0))


def power_4(x):
    return((0, 0, 0, 0, 1, 0, 0, 0))


def power_5(x):
    return((0, 0, 0, 0, 0, 1, 0, 0))


def power_6(x):
    return((0, 0, 0, 0, 0, 0, 1, 0))


def power_7(x):
    return((0, 0, 0, 0, 0, 0, 0, 1))


def power_all(x):
    return((1, 1, 1, 1, 1, 1, 1, 1))


# lamps


def lamp_0(x):
    return((x[0], 0, 0, 0, 0, 0, 0, 0))


def lamp_1(x):
    return((0, x[1], 0, 0, 0, 0, 0, 0))


def lamp_2(x):
    return((0, 0, x[2], 0, 0, 0, 0, 0))


def lamp_3(x):
    return((0, 0, 0, x[3], 0, 0, 0, 0))


def lamp_4(x):
    return((0, 0, 0, 0, x[4], 0, 0, 0))


def lamp_5(x):
    return((0, 0, 0, 0, 0, x[5], 0, 0))


def lamp_6(x):
    return((0, 0, 0, 0, 0, 0, x[6], 0))


def lamp_7(x):
    return((0, 0, 0, 0, 0, 0, 0, x[7]))


# wires


def wire_0_4(x):
    return((x[4], 0, 0, 0, x[0], 0, 0, 0))


def wire_1_5(x):
    return((0, x[5], 0, 0, 0, x[1], 0, 0))


def wire_2_6(x):
    return((0, 0, x[6], 0, 0, 0, x[2], 0))


def wire_3_7(x):
    return((0, 0, 0, x[7], 0, 0, 0, x[3]))


# diodes


def diode_4_0(x):
    return((x[4], 0, 0, 0, 0, 0, 0, 0))


def diode_0_2(x):
    return((0, 0, x[0], 0, 0, 0, 0, 0))


def diode_0_4(x):
    return((0, 0, 0, 0, x[0], 0, 0, 0))


def memory_diode_0_4_0(x):
    return((x[0], 0, 0, 0, x[0], 0, 0, 0))


# switches


def switch_0_4_3(x):
    if x[3]:
        return((0, 0, 0, 0, 0, 0, 0, 0))
    else:
        return((x[4], 0, 0, 0, x[0], 0, 0, 0))


# memory


def memory(x):
    return((x[0], x[1], x[2], x[3], x[4], x[5], x[6], x[7]))


# life-like cellular automata


def seeds(x):
    neighbors = 0
    for i in x:
        neighbors += i

    if neighbors == 2:
        return((1, 1, 1, 1, 1, 1, 1, 1))

    else:
        return((0, 0, 0, 0, 0, 0, 0, 0))


# one-dimensional cellular automata


def rule110(x):
    if (x[7], x[6], x[5]) in ((1, 1, 0), (1, 0, 1), (0, 1, 1), (0, 1, 0), (0 ,0, 1)):
        return((0, 1, 1, 1, 0, 0, 0, 0))
    else:
        return((0, 0, 0, 0, 0, 0, 0, 0))


def rule184(x):
    if (x[7], x[6], x[5]) in ((1, 1, 1), (1, 0, 1), (1, 0, 0), (0, 1, 1)):
        return((0, 1, 1, 1, 0, 0, 0, 0))
    else:
        return((0, 0, 0, 0, 0, 0, 0, 0))


# random


def rand(x):
    inp = 0

    for i in range(8):
        inp += x[i] * 2 ** i

    return(random_rule[inp])


def rand_0(x):
    inp = 0

    for i in range(8):
        inp += x[i] * 2 ** i

    return(random_rule_0[inp])


## models ##

# surface tension

def magnet(x):
    if 1 in (x[4], x[5], x[6], x[7]):
        return((1, 1, 1, 1, 0, 0, 0, 0))
    else:
        return((0, 0, 0, 0, 1, 1, 1, 1))


def antimagnet(x):
    if 1 in (x[0], x[1], x[2], x[3]):
        return((0, 0, 0, 0, 1, 1, 1, 1))
    else:
        return((1, 1, 1, 1, 0, 0, 0, 0))

## neurons ##

# lateral inhibition


def lat_inh(x):
    inp = x[7] + x[6] + x[5]

    if inp >= 2 and x[0] == 0 and x[4] == 0:
        return((1, 1, 1, 1, 1, 0, 0, 0))

    else:
        return((0, 0, 0, 0, 0, 0, 0, 0))

## wolfram ##

# wolfram 1


def wolfram_1(x):
    #b5
    if x[2] == 1 and x[3] == 1 and x[4] == 1:
        return((0, 0, 0, 0, 0, 0, 0, 0))
    #a4
    if x[0] == 1 and x[6] == 1:
        return((0, 0, 0, 0, 0, 0, 1, 1))
    #b2
    elif x[0] == 1 and x[5] == 1:
        return((0, 0, 0, 0, 1, 0, 1, 0))
    #a3
    elif x[1] == 1:
        return((0, 1, 1, 0, 0, 0, 0, 0))
    #a2
    elif x[0] == 1:
        return((0, 0, 0, 0, 1, 1, 0, 0))
    #b3
    elif x[2] == 1:
        return((1, 0, 1, 0, 0, 0, 0, 0))
    #a1
    else:
        return((0, 0, 0, 0, 1, 0, 0, 0))
