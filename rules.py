import random
import copy
from collections import deque
from cells import *
from logicells import *
from biochem import *

colors = [{"0": "black", "1": "white", "2": "red", "3": "green", "4": "blue", "5": "yellow", "6": "brown", "7": "orange", "8": "pink", "9": "purple", "10": "grey"}]
bg_black = [{"None": "black"}]
bg_grey = [{"None": "grey"}, {"None": "grey"}]
biochem = [{"None": "blue", "Cytosol": "white", "DNA": "red", "Ribosome": "black", "Vesicle": "grey"}]

def number_to_base(number: int, base: int, length: int):
    """return string of base-10 number converted to arbitrary base 0-padded with given length"""

    if number == 0:
        return '0'.zfill(length)
    digits = ''
    while number:
        digits = '{:x}'.format(number % base) + digits
        number //= base
    return digits.zfill(length)

class LogiCell(Cell):
    """Base class of Logical CA rule"""

    def __init__(self, rule: str):
        super().__init__()
        self.rule = rule

    def __repr__(self):
        return str(self.rule)

    def __str__(self):
        if self.state is not None:
            value = 0
            for n in self.state:
                value += n * 32
            return str(value - 1)
        else:
            return 'None'


class Anisotropic(Rule):
    """Rule for anisotropic CA"""

    def __init__(self, height, width, pixel=5, colors=colors, fill=0.01, states=2, neighborhood='moore1', zero=True,
                 tame=0.85, rand=0.0):
        super().__init__(height, width, pixel, colors, neighborhood)
        self.name = 'Anisotropic'
        self.fill = fill
        self.states = [n for n in range(states)]
        self.rand = rand
        self.zero = zero
        self.tame = tame

    def text(self):
        n_states = len(self.states)

        return {'name': self.name,
                'dimensions': str(self.height) + ' x ' + str(self.width),
                'pixel': str(self.pixel),
                'fill': str(self.fill),
                'states': str(n_states),
                'neighborhood': str(self.neighborhood),
                'zero': str(self.zero),
                'tame': str(self.tame),
                'rand': str(self.rand)}

    def save(self):
        return {'name': self.name,
                'transitions': self.transitions,
                'states': self.states,
                'neighborhood': self.neighborhood,
                'zero': self.zero,
                'tame': self.tame}

    def load(self, imp):
        self.transitions = imp['transitions']
        self.states = imp['states']
        self.neighborhood = imp['neighborhood']
        self.coord()
        self.zero = imp['zero']
        self.tame = imp['tame']

        self.randomize_grid(self.states, self.fill)

    def reset(self):
        self.transitions = []

        print('generating rule...')

        for n in range(len(self.states) ** len(self.coordlist)):
            if random.random() < self.tame:
                self.transitions.append(0)
            else:
                self.transitions.append(random.choice(self.states[1:]))

        if self.zero:
            self.transitions[0] = 0

        print(len(self.transitions), 'transition rules generated!')

        self.randomize_grid(self.states, self.fill)

    def right_click(self, y: int, x: int, state_copy):
        if state_copy is None:
            state = input("state: ")
            self.grid[y][x] = int(state) % len(self.states)
        else:
            self.grid[y][x] = state_copy

    def tic(self, y: int, x: int):
        neighbors = self.return_neighbors(self.coordlist, y, x)
        draw = True

        if self.rand != 0 and random.random() < self.rand:
            neighbors[self.center] = random.choice(self.states)
            self.grid[y][x] = neighbors[self.center]

        center_value = neighbors[self.center]

        if center_value == self.nextgrid[y][x]:
            draw = False

        value = 0
        for n, neighbor in enumerate(neighbors):
            value += neighbor * len(self.states) ** n

        self.nextgrid[y][x] = self.transitions[value]

        if draw:
            return [(y, x, center_value)]
        else:
            return []

    def swap(self):
        self.grid, self.nextgrid = self.nextgrid, self.grid


class Isotropic(Rule):
    """Rule for isotropic CA"""

    def __init__(self, height, width, pixel=5, colors=colors, fill=0.01, states=2, neighborhood='moore1', zero=True,
                 tame=0.85, rand=0.0, radius=1):
        super().__init__(height, width, pixel, colors, neighborhood)
        self.name = 'Isotropic'
        self.fill = fill
        self.states = [n for n in range(states)]
        self.rand = rand
        self.zero = zero
        self.tame = tame
        self.radius = radius

    def text(self):
        n_states = len(self.states)

        return {'name': self.name,
                'dimensions': str(self.height) + ' x ' + str(self.width),
                'pixel': str(self.pixel),
                'fill': str(self.fill),
                'states': str(n_states),
                'neighborhood': str(self.neighborhood),
                'zero': str(self.zero),
                'tame': str(self.tame),
                'rand': str(self.rand),
                'radius': str(self.radius)}

    def save(self):
        return {'name': self.name,
                'transitions': self.transitions,
                'states': self.states,
                'neighborhood': self.neighborhood,
                'zero': self.zero,
                'tame': self.tame,
                'radius': self.radius}

    def load(self, imp):
        self.transitions = imp['transitions']
        self.states = imp['states']
        self.neighborhood = imp['neighborhood']
        self.coord()
        self.zero = imp['zero']
        self.tame = imp['tame']
        self.radius = imp['radius']

        self.randomize_grid(self.states, self.fill)

    def reset(self):
        self.transitions = {}

        print('generating rule...')

        for state in self.states:
            for n in range(len(self.states) ** (len(self.coordlist) - 1)):
                digits = deque(number_to_base(n, len(self.states), len(self.coordlist) - 1))

                if random.random() < self.tame:
                    value = 0
                else:
                    value = random.choice(self.states[1:])

                for coord in range(len(self.coordlist) - 1):
                    digits.rotate(self.radius)
                    self.transitions.setdefault(''.join(digits) + str(state), value)

        if self.zero:
            self.transitions['0'.zfill(len(self.coordlist))] = 0

        print(len(self.transitions), 'transition rules generated!')

        self.randomize_grid(self.states, self.fill)

    def right_click(self, y: int, x: int, state_copy):
        if state_copy is None:
            state = input("state: ")
            self.grid[y][x] = int(state) % len(self.states)
        else:
            self.grid[y][x] = state_copy

    def tic(self, y: int, x: int):
        neighbors = self.return_neighbors(self.coordlist, y, x)
        draw = True

        if self.rand != 0 and random.random() < self.rand:
            neighbors[self.center] = random.choice(self.states)
            self.grid[y][x] = neighbors[self.center]

        center_value = neighbors[self.center]

        del neighbors[self.center]

        if center_value == self.nextgrid[y][x]:
            draw = False

        self.nextgrid[y][x] = self.transitions[''.join(map(str, neighbors)) + str(center_value)]

        if draw:
            return [(y, x, center_value)]
        else:
            return []

    def swap(self):
        self.grid, self.nextgrid = self.nextgrid, self.grid


class Totalistic(Rule):
    """Rule for totalistic ("Life-like") CA"""

    def __init__(self, height, width, pixel=5, colors=colors, fill=0.06, states=2, neighborhood='moore1', bs='3/23',
                 rand=0.0):
        super().__init__(height, width, pixel, colors, neighborhood)
        self.name = 'Totalistic'
        self.fill = fill
        self.states = [n for n in range(states)]
        self.rand = rand
        b, s = bs.split('/')
        self.birth = tuple([int(n) for n in b])
        self.survival = tuple([int(n) for n in s])

    def text(self):
        n_states = len(self.states)

        rulestring = 'B'
        for b in self.birth:
            rulestring += str(b)
        rulestring += '/S'
        for s in self.survival:
            rulestring += str(s)

        return {'name': self.name,
                'dimensions': str(self.height) + ' x ' + str(self.width),
                'pixel': str(self.pixel),
                'fill': str(self.fill),
                'states': str(n_states),
                'neighborhood': str(self.neighborhood),
                'rulestring': rulestring,
                'rand': str(self.rand)}

    def save(self):
        return {'name': self.name,
                'states': self.states,
                'neighborhood': self.neighborhood,
                'birth': self.birth,
                'survival': self.survival}

    def load(self, imp):
        self.states = imp['states']
        self.neighborhood = imp['neighborhood']
        self.coord()
        self.birth = imp['birth']
        self.survival = imp['survival']
        self.reset()

    def reset(self):
        self.randomize_grid(self.states, self.fill)

    def right_click(self, y: int, x: int, state_copy):
        if state_copy is None:
            state = input("state: ")
            self.grid[y][x] = int(state) % len(self.states)
        else:
            self.grid[y][x] = state_copy

    def tic(self, y: int, x: int):
        neighbors = self.return_neighbors(self.coordlist, y, x)
        draw = True

        if self.rand != 0 and random.random() < self.rand:
            center_value = random.choice(self.states)
            self.grid[y][x] = center_value
        else:
            center_value = neighbors[self.center]
        del neighbors[self.center]

        if center_value == self.nextgrid[y][x]:
            draw = False

        n_state = [neighbors.count(state) for state in self.states[1:]]

        if center_value == 0:
            born = [self.states[state + 1] for state, n in enumerate(n_state) if n in self.birth]
            if len(born) > 0:
                self.nextgrid[y][x] = self.states[random.choice(born)]
            else:
                self.nextgrid[y][x] = 0
        elif center_value != 0 and sum(n_state) in self.survival:
            self.nextgrid[y][x] = center_value
        else:
            self.nextgrid[y][x] = 0

        if draw:
            return [(y, x, center_value)]
        else:
            return []

    def swap(self):
        self.grid, self.nextgrid = self.nextgrid, self.grid


class Genetic(Rule):
    """Competitive / genetic algorithm for Life-like CA"""

    def __init__(self, height, width, pixel=4, colors=bg_black, fill=0.01, neighborhood='moore1', uv='0/0', rand=0.0):
        super().__init__(height, width, pixel, colors, neighborhood)
        self.name = 'Genetic'
        self.fill = fill
        self.rand = rand
        u, v = uv.split('/')
        self.u = (int(u) + 1) if int(u) < len(self.coordlist) else len(self.coordlist)
        self.v = int(v) if int(v) < len(self.coordlist) else len(self.coordlist)
        self.max_birth = [b + self.u for b, coord in enumerate(self.coordlist[self.u:])]
        self.max_survival = [s for s, coord in enumerate(self.coordlist[self.v:])]

    def text(self):
        limits = str(self.u - 1) + '/' + str(self.v)

        return {'name': self.name,
                'dimensions': str(self.height) + ' x ' + str(self.width),
                'pixel': str(self.pixel),
                'fill': str(self.fill),
                'neighborhood': str(self.neighborhood),
                'limits': limits,
                'mutation rate': str(self.rand)}

    def save(self):
        return {'name': self.name,
                'neighborhood': self.neighborhood,
                'u': self.u,
                'v': self.v}

    def load(self, imp):
        self.neighborhood = imp['neighborhood']
        self.coord()
        self.u = imp['u']
        self.v = imp['v']
        self.max_birth = [b + self.u for b, coord in enumerate(self.coordlist[self.u:])]
        self.max_survival = [s for s, coord in enumerate(self.coordlist[self.v:])]
        self.reset()

    def reset(self):
        for y in range(self.height):
            for x in range(self.width):
                if random.random() < self.fill:
                    self.grid[y][x] = self.random_state()
                else:
                    self.grid[y][x] = None

    def random_state(self):
        birth = [b for b in self.max_birth if random.random() < 0.5]
        survival = [s for s in self.max_survival if random.random() < 0.5]

        self.colors[0].setdefault(str((birth, survival)), random.randint(1, 255 ** 3))

        return((birth, survival))

    def mutate(self, state: tuple):
        state = copy.deepcopy(state)
        if random.random() < 0.5:
            b = random.choice(self.max_birth)
            if b not in state[0]:
                state[0].append(b)
                state[0].sort()
            else:
                state[0].remove(b)
        else:
            s = random.choice(self.max_survival)
            if s not in state[1]:
                state[1].append(s)
                state[1].sort()
            else:
                state[1].remove(s)

        self.colors[0].setdefault(str(state), random.randint(1, 255 ** 3))

        return state

    def right_click(self, y: int, x: int, state_copy):
        if state_copy is None:
            birth = [int(n) for n in input("birth: ")]
            survival = [int(n) for n in input("survival: ")]
            birth.sort()
            survival.sort()
            state = (birth, survival)

            self.colors[0].setdefault(str(state), random.randint(1, 255 ** 3))

        else:
            state = copy.deepcopy(state_copy)

        self.grid[y][x] = state

    def tic(self, y: int, x: int):
        neighbors = self.return_neighbors(self.coordlist, y, x)
        center_value = neighbors[self.center]
        del neighbors[self.center]
        neighbors = [neighbor for neighbor in neighbors if neighbor is not None]
        n_neighbors = len(neighbors)
        draw = True
        if center_value == self.nextgrid[y][x]:
            draw = False

        if center_value is not None:
            if self.rand != 0:
                if random.random() < self.rand:
                    center_value = self.mutate(center_value)
                    self.grid[y][x] = center_value
            if n_neighbors in center_value[1]:
                self.nextgrid[y][x] = center_value
            else:
                self.nextgrid[y][x] = None
        elif center_value is None and n_neighbors > 0:
            birthlist = [neighbor for neighbor in neighbors if n_neighbors in neighbor[0]]
            if len(birthlist) > 0:
                self.nextgrid[y][x] = random.choice(birthlist)
            else:
                self.nextgrid[y][x] = None
        else:
            self.nextgrid[y][x] = None

        if draw:
            return [(y, x, center_value)]
        else:
            return []

    def swap(self):
        self.grid, self.nextgrid = self.nextgrid, self.grid


class Logical(Rule):
    """Rule for simulating logical circuits with CA, see logicells.py"""

    def __init__(self, height, width, pixel=50, colors=bg_grey, neighborhood='moore1', palette='red'):
        super().__init__(height, width, pixel, colors, neighborhood)
        self.name = 'Logical'
        self.palette = ['red', 'green', 'blue'].index(palette)

        for n in range(9):
            value = [0, 0, 0]
            value[self.palette] = n * 32 - 1
            self.colors[0][str(n * 32 - 1)] = value
        self.colors[0]['-1'] = [0, 0, 0]

        for n in range(9):
            value = [0, 0, 0]
            value[self.palette] = 255
            self.colors[1][str(n * 32 - 1)] = value
        self.colors[1]['-1'] = [0, 0, 0]

    def text(self):
        return {'name': self.name,
                'dimensions': str(self.height) + ' x ' + str(self.width),
                'pixel': str(self.pixel),
                'neighborhood': str(self.neighborhood)}

    def save(self):
        return {'name': self.name,
                'neighborhood': self.neighborhood,
                'grid': self.grid}

    def load(self, imp):
        self.neighborhood = imp['neighborhood']
        self.coord()
        self.grid = copy.deepcopy(imp['grid'])
        self.nextgrid = copy.deepcopy(imp['grid'])

    def reset(self):
        self.empty()

    def right_click(self, y: int, x: int, state_copy):
        if state_copy is None:
            rule = input('rule: ')
        else:
            rule = state_copy.rule
        try:
            state = globals()[rule]([0 for n in range(8)])
            self.grid[y][x] = LogiCell(rule)
            self.grid[y][x].state = state
            self.nextgrid[y][x] = LogiCell(rule)
        except KeyError:
            print('Rule not found!')

    def tic(self, y: int, x: int):
        if self.grid[y][x] is not None:
            neighbors = self.return_neighbors(self.coordlist, y, x)
            center_value = neighbors[self.center]
            del neighbors[self.center]
            draw = True
            if center_value.state == self.nextgrid[y][x].state:
                draw = False

            inp = []
            for n, neighbor in enumerate(neighbors):
                if neighbor is not None:
                    inp.append(neighbor.state[i])
                else:
                    inp.append(0)
            self.nextgrid[y][x].state = globals()[center_value.rule](inp)

            if draw:
                return [(y, x, center_value)]

        return []

    def swap(self):
        self.grid, self.nextgrid = self.nextgrid, self.grid


class BioChem(Rule):
    """Rule to simulate biochemical processes with CA, see biochem.py"""

    def __init__(self, height, width, pixel=50, colors=biochem, neighborhood='moore1'):
        super().__init__(height, width, pixel, colors, neighborhood)
        self.name = 'BioChem'

    def text(self):
        return {'name': self.name,
                'dimensions': str(self.height) + ' x ' + str(self.width),
                'pixel': str(self.pixel),
                'neighborhood': str(self.neighborhood)}

    def save(self):
        return {'name': self.name,
                'neighborhood': self.neighborhood,
                'grid': self.grid}

    def load(self, imp):
        self.neighborhood = imp['neighborhood']
        self.coord()
        self.grid = copy.deepcopy(imp['grid'])

    def reset(self):
        self.empty()

    def right_click(self, y: int, x: int, state_copy):
        if state_copy is None:
            molecule = input('molecule: ')
        else:
            molecule = str(state_copy)
        try:
            self.grid[y][x] = globals()[molecule]()
        except KeyError:
            print('Molecule not defined!')

    def tic(self, y: int, x: int):
        paint = []

        if self.grid[y][x] is not None:
            neighbors = self.return_neighbors(self.coordlist, y, x)
            molecule = neighbors[self.center]
            del neighbors[self.center]
            while True:
                ry, rx = random.choice(self.coordlist)
                if (ry, rx) != (0, 0):
                    break
            ny, nx = (y + ry) % len(self.grid), (x + rx) % len(self.grid[y])
            new_neighbors = self.return_neighbors(self.coordlist, ny, nx)
            new_center = new_neighbors[self.center]
            new_neighbors = [neighbor for neighbor in new_neighbors if neighbor != molecule]
            move, action = molecule.action(neighbors, new_center, new_neighbors)

            if move:
                self.grid[ny][nx], self.grid[y][x] = self.grid[y][x], self.grid[ny][nx]
                paint.append((y, x, new_center))
                paint.append((ny, nx, molecule))

            if action is not None:
                for a in action:
                    target, subst = a
                    neighbors.insert(self.center, molecule)
                    ind = neighbors.index(target)
                    ay, ax = self.coordlist[ind]
                    nay, nax = (y + ay) % len(self.grid), (x + ax) % len(self.grid[y])
                    self.grid[nay][nax] = subst
                    paint.append((nay, nax, subst))

            if not move and action is None:
                paint.append((y, x, molecule))

        else:
            paint.append((y, x, None))

        return paint

    def swap(self):
        pass
